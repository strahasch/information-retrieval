package de.hsog.lucene.demo.extractor;

import java.net.URL;
import java.util.List;
import java.util.Map;

import net.htmlparser.jericho.CharacterReference;
import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.MasonTagTypes;
import net.htmlparser.jericho.MicrosoftConditionalCommentTagTypes;
import net.htmlparser.jericho.PHPTagTypes;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;

public class HTMLExtractor {

	//public static void main(String[] args) throws Exception {
	public static HTMLDocument extractFrom(String fileString) throws Exception {
		HTMLDocument doc = new HTMLDocument();
		boolean log = true;

		String sourceUrlString = "file:"+fileString;

		MicrosoftConditionalCommentTagTypes.register();
		PHPTagTypes.register();
		PHPTagTypes.PHP_SHORT.deregister(); // remove PHP short tags for this
											// example otherwise they override
											// processing instructions
		MasonTagTypes.register();
		Source source = new Source(new URL(sourceUrlString));

		// Call fullSequentialParse manually as most of the source will be
		// parsed.
		source.fullSequentialParse();

		String title = getTitle(source);
		doc.setTitle(title);

		String description = getMetaValue(source, "description");
		if (description == null){
			description = "";
		}
		doc.setDescription(description);

		String keywords = getMetaValue(source, "keywords");
		if (keywords == null){
			keywords = "";
		}
		doc.setKeywords(keywords);

		List<Element> linkElements = source.getAllElements(HTMLElementName.A);
		for (Element linkElement : linkElements) {
			String href = linkElement.getAttributeValue("href");
			if (href == null)
				continue;
			// A element can contain other tags so need to extract the text from
			// it:
			String label = linkElement.getContent().getTextExtractor()
					.toString();
			doc.links.put(label, href);
		}

		doc.setContent(source.getTextExtractor().setIncludeAttributes(true)
				.toString());

		if (log == true) {
			print(doc);
		}

		return doc;
	}

	// System.out.println("\nSame again but this time extend the TextExtractor class to also exclude text from P elements and any elements with class=\"control\":\n");
	// TextExtractor textExtractor=new TextExtractor(source) {
	// public boolean excludeElement(StartTag startTag) {
	// return startTag.getName()==HTMLElementName.P ||
	// "control".equalsIgnoreCase(startTag.getAttributeValue("class"));
	// }
	// };
	// System.out.println(textExtractor.setIncludeAttributes(true).toString());

	private static String getTitle(Source source) {
		Element titleElement = source.getFirstElement(HTMLElementName.TITLE);
		if (titleElement == null)
			return null;
		// TITLE element never contains other tags so just decode it collapsing
		// whitespace:
		return CharacterReference.decodeCollapseWhiteSpace(titleElement
				.getContent());
	}

	private static String getMetaValue(Source source, String key) {
		for (int pos = 0; pos < source.length();) {
			StartTag startTag = source.getNextStartTag(pos, "name", key, false);
			if (startTag == null)
				return null;
			if (startTag.getName() == HTMLElementName.META)
				return startTag.getAttributeValue("content"); // Attribute
																// values are
																// automatically
																// decoded
			pos = startTag.getEnd();
		}
		return null;
	}

	private static void print(HTMLDocument doc){
		System.out.println("Document title:");
		System.out.println(doc.getContent() == null ? "(none)" : doc.getTitle());

		System.out.println("\nDocument description:");
		System.out.println(doc.getDescription() == null ? "(none)" : doc.getDescription());

		System.out.println("\nDocument keywords:");
		System.out.println(doc.getKeywords() == null ? "(none)" : doc.getKeywords());

		System.out.println("\nLinks to other documents:");
		for (Map.Entry<String, String> e : doc.getLinks().entrySet()) {
			System.out.println(e.getKey() + " -> " + e.getValue());
		}

		System.out
				.println("\nAll text from file (exluding content inside SCRIPT and STYLE elements):\n");
		System.out.println(doc.getContent());
	}

}
