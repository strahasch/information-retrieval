package de.hsog.lucene.demo;

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.index.FieldInfo.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import de.hsog.lucene.demo.extractor.HTMLDocument;

/**
 * Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing. Run
 * it with no command-line arguments for usage information. Kommentare auf
 * Deutsch von ST
 */
public class IndexFiles {

	private IndexFiles() {
	}

	/** Index all text files under a directory. */
	public static void main(String[] args) {

		String basePath = "C:/Lucene/Crawler/";
		String indexPath = basePath + "Index/";
		String docsPath = basePath + "Docs/";
		boolean create = true; // Soll der Index neu erzeugt werden?

		// Check, ob Verzeichnis lesbar
		final File docDir = new File(docsPath);
		if (!docDir.exists() || !docDir.canRead()) {
			System.out
					.println("Document directory '"
							+ docDir.getAbsolutePath()
							+ "' does not exist or is not readable, please check the path");
			System.exit(1);
		}

		// Ab hier wird es interessant ...
		Date start = new Date();
		try {
			System.out.println("Indexing to directory '" + indexPath + "'...");

			Directory dir = FSDirectory.open(new File(indexPath));

			// Analyzer, um die Dokumente zu analysieren
			// Was waren nochmal die Aufgaben eines Analysers?
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_31);

			// Konfiguration des IndexWriters
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_31,
					analyzer);

			if (create) {
				// Create a new index in the directory, removing any
				// previously indexed documents:
				iwc.setOpenMode(OpenMode.CREATE);
			} else {
				// Add new documents to an existing index:
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}

			IndexWriter writer = new IndexWriter(dir, iwc);

			// hier werden nun die Dokumente indexiert
			// schauen Sie sich unbedingt die Methode indexDocs an!
			indexDocs(writer, docDir);

			writer.close();

			Date end = new Date();
			System.out.println(end.getTime() - start.getTime()
					+ " total milliseconds");

		} catch (IOException e) {
			System.out.println(" caught a " + e.getClass()
					+ "\n with message: " + e.getMessage());
		}
	}

	/**
	 * Indexes the given file using the given writer, or if a directory is
	 * given, recurses over files and directories found under the given
	 * directory.
	 *
	 * NOTE: This method indexes one document per input file. This is slow. For
	 * good throughput, put multiple documents into your input file(s). An
	 * example of this is in the benchmark module, which can create "line doc"
	 * files, one document per line, using the <a href=
	 * "../../../../../contrib-benchmark/org/apache/lucene/benchmark/byTask/tasks/WriteLineDocTask.html"
	 * >WriteLineDocTask</a>.
	 *
	 * @param writer
	 *            Writer to the index where the given file/dir info will be
	 *            stored
	 * @param file
	 *            The file to index, or the directory to recurse into to find
	 *            files to index
	 * @throws IOException
	 */
	static void indexDocs(IndexWriter writer, File file) throws IOException {
		// do not try to index files that cannot be read
		if (file.canRead()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				// an IO error could occur
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						indexDocs(writer, new File(file, files[i]));
					}
				}
			} else {

				FileInputStream fis;
				try {
					fis = new FileInputStream(file);
				} catch (FileNotFoundException fnfe) {
					// at least on windows, some temporary files raise this
					// exception with an "access denied" message
					// checking if the file can be read doesn't help
					return;
				}

				try {

					ObjectInputStream o = new ObjectInputStream(fis);
					HTMLDocument hdoc = (HTMLDocument) o.readObject();
					o.close();

					// neues Lucene-Doc erzeugen
					Document doc = new Document();

					// Nun m�ssen Sie die Felder hinzuf�gen.
					// Was sind Fields?
					// Beispiel
					Field url = new Field("url", hdoc.getUrl(),
							Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
					url.setIndexOptions(IndexOptions.DOCS_ONLY);
					doc.add(url);

					// Titel in einem Field speichern
					Field titleField = new Field("title", hdoc.getTitle(), Field.Store.YES, Field.Index.ANALYZED);
					titleField.setBoost(5);
					titleField.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
					doc.add(titleField);

					// Description in einem Field speichern
					Field descriptionField = new Field("description", hdoc.getDescription(), Field.Store.YES, Field.Index.ANALYZED);
					doc.add(descriptionField);

					// Keywords in einem Field speichern
					Field keywordsField = new Field("keywords", hdoc.getKeywords(), Field.Store.YES, Field.Index.ANALYZED);
					doc.add(keywordsField);

					// Inhalt in einem Field speichern
					System.out.println("content "+hdoc.getContent());
					Field contentField = new Field("content", hdoc.getContent(), Field.Store.NO, Field.Index.ANALYZED_NO_NORMS);
					doc.add(contentField);


					if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
						// New index, so we just add the document (no old
						// document can be there):
						System.out.println("adding " + file);
						writer.addDocument(doc);
					} else {
						// Existing index (an old copy of this document may have
						// been indexed) so
						// we use updateDocument instead to replace the old one
						// matching the exact
						// path, if present:
						System.out.println("updating " + file);
						writer.updateDocument(new Term("path", file.getPath()),
								doc);
					}

				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} finally {
					fis.close();
				}
			}
		}
	}
}
