package de.hsog.ir;

import java.util.Vector;

public class TermInformation {
	String term;
	int count;
	Vector<DocID> postings;


	public TermInformation(String term, int count, Vector<DocID> postings) {
		this.term = term;
		this.count = count;
		this.postings = postings;
	}

	public void addPosting(DocID id){
		this.postings.add(id);
	}

	public Vector<DocID> getPostings() {
		return postings;
	}

	public boolean equals(Object obj) {
		TermInformation strInput = (TermInformation) obj;
		return term.equals(strInput.term);
	}

	public String toString() {
		String result;
		result = new String(term + " " + count + " ");
		for (int i = 0; i < postings.size(); i++) {
			result += ("" + postings.elementAt(i) + " ");
		}
		return result;
	}
}

class WordInformationCompare implements Compare {
	public boolean lessThan(Object l, Object r) {
		TermInformation w1 = (TermInformation) l;
		TermInformation w2 = (TermInformation) r;

		return w1.term.compareToIgnoreCase(w2.term) < 0;
	}

	public boolean lessThanOrEqual(Object l, Object r) {
		TermInformation w1 = (TermInformation) l;
		TermInformation w2 = (TermInformation) r;

		return w1.term.compareToIgnoreCase(w2.term) <= 0;
	}
}
