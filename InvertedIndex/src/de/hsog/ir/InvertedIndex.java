package de.hsog.ir;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Vector;

public class InvertedIndex {

	SortVector index = new SortVector(new WordInformationCompare());
	StopWordList stopWords;

	public InvertedIndex(String stopwordfilename) {
		stopWords = new StopWordList(stopwordfilename);
	}

	public void addTerm(String term, int documentId) {
		DocID docid = new DocID(documentId);

		// TO DO
		// skip stop words
		// good time to stem is here
		// String porterStem = porterStemmer.runStemmer(token);
		// String lovinsStem = lovinsStemmer.stemString(token);

		if (!stopWords.contains(term)) {
			System.out.println("Add term:" + term + " to inverted index with "
					+ documentId);
			TermInformation terminfo = new TermInformation(term, 1,
					new Vector<DocID>());
			// checks if inverted index contains term
			int idx = index.indexOf(terminfo);
			// no, then add term
			if (idx < 0) {
				terminfo.addPosting(docid);
				index.add(terminfo);
			}
			// yes, then update posting
			else {
				terminfo = (TermInformation) index.elementAt(idx);
				if (terminfo.getPostings().indexOf(docid) < 0) {
					terminfo.addPosting(docid);
					terminfo.count++;
				}
			}
		}
	}

	public void extractTokens(String data, int docid) throws IOException {

		StringReader in = new StringReader(data);
		StringBuffer sb = new StringBuffer();

		// read chars from document
		// i = -1 at end of file (eof)
		int i = in.read();
		while (i >= 0) {
			// check if character is letter or digit?
			// yes, then read next character
			if (Character.isLetterOrDigit((char) i)) {
				sb.append((char) i);
			}
			// no, then add token to index
			else if (sb.length() > 0) {
				String token = sb.toString();
				System.out.println("Extract word: " + token + " from file"
						+ docid);
				addTerm(token, docid);

				sb.setLength(0);
			}
			i = in.read();
		}
		if (sb.length() > 0)
			addTerm(sb.toString(), docid);
		in.close();
	}

	public void extractTokensFromHTML(String data, int docid)
			throws IOException {

		StringReader in = new StringReader(data);
		StringBuffer sb = new StringBuffer();
		int state = 0;

		int i = in.read();
		while (i >= 0) {
			switch (state) {
			case '<':
				if (i == '>')
					state = 0;
				break;
			case 0:
				if (Character.isLetterOrDigit((char) i)) {
					sb.append((char) i);
				} else {
					if (sb.length() > 0) {
						addTerm(sb.toString(), docid);
						sb.setLength(0);
					}
					if (i == '<') {
						state = '<';
					}
				}
			}
			i = in.read();
		}
		if (sb.length() > 0)
			addTerm(sb.toString(), docid);
		in.close();
	}

	public void saveToFile(String filename) throws FileNotFoundException,
			IOException {
		index.sort();

		DataOutputStream out = new DataOutputStream(new FileOutputStream(
				filename));
		for (int i = 0; i < index.size(); i++) {
			out.writeBytes(index.elementAt(i) + "\n");
		}
		out.close();
	}

}
