# README #

This repository contains source code for the lecture [DB2 Information Retrieval](https://ks.informatik.hs-offenburg.de/lehre/information-retrieval/) at the University of Applied Sciences Offenburg. Lecturer is [Prof. Dr. Stephan Trahasch](http://ks.informatik.hs-offenburg.de/)

## (Eclipse) Projects ##

+ **NaiveSearch**
    + inverted index 
    + term document matrix
    + performance measurement


+ **Lucene Demo 1 Einfuehrung**
    + Basic usage of Lucene
    + contains also [Luke](https://github.com/DmitryKey/luke/) for analyzing the index
    + Corpus: speeches of Barack Obama and Shakespeare

### How do I get set up? ###

* Clone repository or download 
* Import projects for example Lucene Demo 1 Einfuehrung