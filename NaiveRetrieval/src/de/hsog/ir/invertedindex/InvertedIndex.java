package de.hsog.ir.invertedindex;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import de.hsog.ir.termdocmatrix.Corpus;
import de.hsog.ir.termdocmatrix.InformationRetrieval;

/**
 * Implementierung des InformationRetrieval-Interface mit einem invertierten
 * Index, basierend auf einem SortedSet.
 */
public class InvertedIndex implements InformationRetrieval {

	// Ein vorkonfigurierter Praeprozessor: Porter-Stemmer 
	private static final PreprocessorWithPorter PREPROCESSOR = new PreprocessorWithPorter();
	// Der Intersection-Algorithmus von Manning et al.:
	private static final Intersection INTERSECTION = new IntersectionBook();

	// der eigentliche InvertedIndex
	Map<String, SortedSet<Integer>> index = new HashMap<String, SortedSet<Integer>>();
	
	// Stoppwort-Liste
	StopWordList stopWords;
	boolean stopword = false;
	
	//Zaehler fuer Postings
	int countPostings = 0;
	
	//Zaehler fuer Terme
	int countTerms = 0;
	
	
	/**
	 * Creates an InvertedIndex
	 * @param corpus  contains text to index
	 * @param usePorter if true we will use PorterStemmer for Stemming
	 * @param stopwordFilename if <> "" we will use a stop word list
	 */
	public InvertedIndex(Corpus corpus, boolean usePorter, String stopwordFilename) {

		// Wir erzeugen den Index aus dem Korpus:
		long start = System.currentTimeMillis();

		// Stoppwort-Liste aufbauen, falls ein Dateiname uebergeben wurde
		if (!stopwordFilename.isEmpty()) {
			stopWords = new StopWordList(stopwordFilename);
			stopword = true;
		}

		index = index(corpus, usePorter);
		System.out.println(String.format(
				"Preprocessing index with %s terms took %s ms.", index.keySet()
						.size(), System.currentTimeMillis() - start));

	}

	/*
	 * erzeugt einen InvertedIndex fuer einen Corpus
	 */
	public Map<String, SortedSet<Integer>> index(Corpus corpus,
			boolean usePorter) {
		List<String> works = corpus.getWorks();
		Map<String, SortedSet<Integer>> index = new HashMap<String, SortedSet<Integer>>();

		/* Wir indizieren jedes Werk/jede Rede: */
		for (int i = 0; i < works.size(); i++) {

			/* Wir benutzen den neuen Preprocessor bei der Indizierung: */
			List<String> terms = PREPROCESSOR.process(works.get(i), usePorter);

			for (String term : terms) {
				/*
				 * nur indizieren, wenn
				 * (stopword = true AND term NICHT in der Stoppwort-Liste enthalten ist)
				 * ODER
				 * stopword = false
				 */
				if ((!stopword) || (stopword && !stopWords.contains(term))) {

					SortedSet<Integer> postings = index.get(term);
					/*
					 * Falls wir noch keine Postings-Liste fuer den aktuellen
					 * Term haben, legen wir eine an:
					 */
					if (postings == null) {
						postings = new TreeSet<Integer>();
						index.put(term, postings);
						countTerms++;
					}
					/*
					 * Wir indizieren den Term, indem wir die ID des
					 * entsprechenden Dokuments in die passende PostingsList
					 * einfuegen:
					 */
					postings.add(i);
					countPostings++;
				}
			}
		}
		/*
		 * Wir koennen zum Testen die Terme ausgeben und sehen dass mit unserem
		 * Praeprozessor der Index inzwischen ganz brauchbar ist:
		 */
		// printSortedIndexTerms(index);
		return index;
	}

	@SuppressWarnings("unused")
	// optional
	private void printSortedIndexTerms(Map<String, SortedSet<Integer>> index) {
		SortedSet<String> keys = new TreeSet<String>(index.keySet());
		for (String string : keys) {
			System.out.println(string);
		}
	}

	/*
	 * Suche ohne Porter
	 * 
	 * @see
	 * de.hsog.ir.boole.aufgabe1.InformationRetrieval#search(java.lang.String)
	 */
	public Set<Integer> search(String query) {
		return search(query, false);
	}

	/*
	 * Suche mit Porter wenn usePorter = true
	 */
	public Set<Integer> search(String query, boolean usePorter) {
		long start = System.currentTimeMillis();
		/*
		 * Wir verarbeiten auch die Suchanfrage mit dem Präprozessor und
		 * koennen so Dinge einheitlich behandeln (z.B. alles lower-case
		 * machen).
		 */
		List<String> queries = PREPROCESSOR.process(query, usePorter);
		/*
		 * Damit wir die Effizienz des Algorithmus aus Manning et al erreichen,
		 * muessen die einzelnen Postings-Listen nach Laenge sortiert sein. Dazu
		 * holen wir uns zunaechst die Listen:
		 */
		List<SortedSet<Integer>> allPostings = new ArrayList<SortedSet<Integer>>();
		for (String q : queries) {
			SortedSet<Integer> postings = index.get(q);
			allPostings.add(postings);
		}
		/* Sortieren diese dann nach ihrer Laenge: */
		Collections.sort(allPostings, new Comparator<SortedSet<Integer>>() {
			public int compare(SortedSet<Integer> o1, SortedSet<Integer> o2) {
				return Integer.valueOf(o1.size()).compareTo(o2.size());
			}
		});
		/*
		 * Ergebnis ist die Schnittmenge (Intersection) der ersten Liste... Hier
		 * behandeln wir die Suchwoerter als UND-Verknuepft!
		 */
		SortedSet<Integer> result = allPostings.get(0);
		/* ...mit allen weiteren: */
		for (SortedSet<Integer> set : allPostings
				.subList(1, allPostings.size())) {
			result = INTERSECTION.of(result, set);
		}
		/*
		 * Um wirklich zu sehen, ob Term-Dokument-Matrix und Postings-Listen
		 * unterschiedliche Laufzeit haben, muesste man System.nanoTime
		 * verwenden; mit Millisekunden sehen wir aber, dass fuer unseren
		 * Anwendungsfall es keinen Unterschied macht.
		 */
		System.out.println(String.format("Search for '%s' took %s ms.", query,
				System.currentTimeMillis() - start));
		return result;
	}

	public void saveToFile(String filename) throws FileNotFoundException,
			IOException {

		Map<String, SortedSet<Integer>> sortedMap = new TreeMap<String, SortedSet<Integer>>(
				this.index);

		BufferedWriter outputStream = new BufferedWriter(new FileWriter(
				filename));

		for (String term : sortedMap.keySet()) {
			// System.out.println("Term: "+term);
			outputStream.write(term + " ");
			SortedSet<Integer> postings = sortedMap.get(term);

			for (Iterator<Integer> i = postings.iterator(); i.hasNext();) {
				Integer post = (Integer) i.next();
				outputStream.write(post + " ");
			}
			outputStream.write("\n");
		}
		outputStream.close();
	}

}
