package de.hsog.ir.invertedindex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/*
 * Hier eine Loesung unter Nutzung der Java-API: kompakt, aber Komplexitaet nicht in unserer
 * Hand (fuer's praktische Programmieren etwas gutes - die Java-Bibliotheken sind schnell und
 * werden laufend optimiert - bei besonderem Bedarf ev. nicht genug)
 */
public class IntersectionAPI implements Intersection {
	public SortedSet<Integer> of(SortedSet<Integer> pl1, SortedSet<Integer> pl2) {
		List<Integer> result = new ArrayList<Integer>(pl1);
		result.retainAll(pl2);
		Collections.sort(result);
		return new TreeSet<Integer>(result);
	}
}
