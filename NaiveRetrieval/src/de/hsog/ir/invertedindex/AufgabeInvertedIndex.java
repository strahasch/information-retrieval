package de.hsog.ir.invertedindex;

import java.io.FileNotFoundException;
import java.io.IOException;

import de.hsog.ir.termdocmatrix.Corpus;
/**
 * Erstellt einen InvertedIndex und
 * speichert diesen ab.
 * 
 * @author Trahasch
 *
 */
public class AufgabeInvertedIndex {
	
	final static boolean usePorter = false;
    
    public static void main (String[] args){

    	InvertedIndex shakespeare = createCorpusShakespeare();
        InvertedIndex obama = createCorpusObama();
        
        //TODO: Suchanfragen an den Index stellen
        //obama.search(query, usePorter);
    }
    
    public static InvertedIndex createCorpusShakespeare(){
    	
    	// Demo mit Werken von Shakespeare
    	String workingDirectory = "data//";
    	String textFile = "data/shakspeare12.txt";
		//String stopwordFile = "data/stops.txt";
    	String stopwordFile = "";
    	
    	Corpus corpus = new Corpus(textFile, "1[56][0-9][0-9]");
		System.out.println("Corpus: William Shakespeare");
		System.out.println("Corpus work size: " + corpus.getWorks().size());
		
    	InvertedIndex ir = new InvertedIndex(corpus, usePorter, stopwordFile);
    	System.out.println("Terme: "+ir.countTerms);
    	System.out.println("Postings: "+ir.countPostings);

        // InvertedIndex wird als txt-File gespeichert
        String indexFilename = workingDirectory + "indexShakespeare.txt";

        try {
			ir.saveToFile(indexFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return ir;
    }

    public static InvertedIndex createCorpusObama(){
		
    	// Demo mit Reden von Barack Obama    	
    	String workingDirectory = "data//";
		String corpusDir = "data/obama";
		//String stopwordFile = "";
		String stopwordFile = "data/stops.txt";
		
		Corpus corpus = new Corpus(corpusDir);
		System.out.println("Corpus: Barack Obama");
		System.out.println("Corpus work size: " + corpus.getWorks().size());
    	InvertedIndex ir = new InvertedIndex(corpus, usePorter, stopwordFile);
    	System.out.println("Terme: "+ir.countTerms);
    	System.out.println("Postings: "+ir.countPostings);

        // InvertedIndex wird als txt-File gespeichert
        String indexFilename = workingDirectory + "indexObama.txt";

        try {
			ir.saveToFile(indexFilename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return ir;
    }
}
