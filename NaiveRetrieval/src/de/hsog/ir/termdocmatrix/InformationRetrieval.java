package de.hsog.ir.termdocmatrix;

import java.util.Set;

/*
 * Information-Retrieval in der einfachsten Form: Ermitteln einer Menge von Dokumenten-IDs, die einer Suchanfrage
 * entsprechen.
 */
public interface InformationRetrieval {

    Set<Integer> search(String query);

}
