package de.hsog.ir.termdocmatrix;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
 * Eine Klasse zur Repraesentation des Korpus und zum Zugriff auf die Werke und die Woerter des Korpus. Im Konstruktor
 * werden der Ort der Textdatei und das Muster, mit dem die einzelnen Dokumente des Korpus getrennt werden sollen,
 * uebergeben.
 */
public class Corpus {

	private List<String> works = new ArrayList<String>();
	
	public Corpus(String location) {

		File directory = new File(location);

		// get all the files from a directory
		File[] fList = directory.listFiles();

		for (File file : fList) {
			if (file.isFile()) {

				StringBuilder builder = new StringBuilder();

				try {
					Scanner scanner = new Scanner(file);
					while (scanner.hasNextLine()) {
						/* Wir lesen Zeilen, entfernen Leerzeichen am Anfang und Ende und fuegen Zeilenenden wieder ein: */
						String current = scanner.nextLine();
						current = current.replaceAll("\\r|\\n", "");
						current = current.trim();
						if (!current.isEmpty()){
							builder.append(current).append("\n");
						}
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				String text = builder.toString();
				this.works.add(text);
				
			}
		}
	}

	public Corpus(String location, String regex) {
		StringBuilder builder = new StringBuilder();

		try {
			Scanner scanner = new Scanner(new File(location));
			while (scanner.hasNextLine()) {
				/* Wir lesen Zeilen und fuegen Zeilenenden wieder ein: */
				builder.append(scanner.nextLine()).append("\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String text = builder.toString();
		this.works = Arrays.asList(text.split(regex));
	}

	public List<String> getWorks() {
		return works;
	}

}
