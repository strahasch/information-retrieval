package de.hsog.ir.termdocmatrix;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * Implementierung von Information Retrieval ueber lineare Suche in allen Texten, 
 * mit einer Laufzeit von O(p*q) bei
 * einer Textlaenge von p (m Werke * n Woerter pro Werk) und q Suchwoertern.
 */
public class LinearSearch implements InformationRetrieval {

	/*
	 * Liste works enth�lt alle Werke von Shakespeare pro Eintrag ein Werk als
	 * String
	 */
	private List<String> works;

	public LinearSearch(Corpus corpus) {
		this.works = corpus.getWorks();
	}

	public Set<Integer> search(String query) {
		long start = System.currentTimeMillis();
		Set<Integer> result = new HashSet<Integer>();

		// Queryterme werden in Array queries aufgesplittet
		
		List<String> queries = Arrays.asList(query.split(" "));

		/*
		 * Wir betrachten jedes Wort in jedem Werk und vergleichen es mit jedem
		 * Suchbegriff, woraus sich die Laufzeitkomplexitaet von O(p*q) ergibt,
		 * d.h. die Laege des Gesamttextes mal die Anzahl der Suchbegriffe.
		 */
		for (int i = 0; i < works.size(); i++) { // m
			// f�r jedes Werk works.get(i) werden alle Worte in der Liste words
			// gespeichert
			List<String> words = Arrays.asList(works.get(i).split(" "));

			for (String word : words) { // m * n = p
				for (String q : queries) { // p * q

					// TODO: Implementieren Sie eine AND-Suche
					if (word.equals(q.trim())) {
						result.add(i); // Implements OR
						break;
					}
				}
			}
		}
		System.out.println("Search took "
				+ (System.currentTimeMillis() - start) + " ms.");
		System.out.println("Linear Search: Found " + query.toString()
				+ " in following documents " + result.toString());
		return result;
	}

}
