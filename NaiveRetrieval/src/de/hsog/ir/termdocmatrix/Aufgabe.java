package de.hsog.ir.termdocmatrix;

import java.util.HashSet;

/**
 * Tests zu Demonstration der Funktionalitaeten: Korpus,
 * lineare Suche und Suche mit Term-Dokument-Matrix; dazu ein paar
 * Laufzeitmessungen zur Verdeutlichung des Unterschieds zwischen O(n*m), d.h.
 * Textlaenge mal Anzahl der Suchbegriffe, und O(1), d.h. konstanter Laufzeit,
 * die nach Aufbau der Matrix unabhaengig von der Textlaenge ist.
 * 
 */
public class Aufgabe {

	public static void main(String[] args) {

		Corpus corpus;

		// Demo mit Werken von Shakespeare
		String textFile = "data/shakspeare12.txt";
		corpus = new Corpus(textFile, "1[56][0-9][0-9]");
		System.out.println("Corpus: William Shakespeare");
		System.out.println("Corpus work size: " + corpus.getWorks().size());

		// zuerst die naive lineare Suche
		LinearSearch ls = new LinearSearch(corpus);
		linear(ls, "Brutus");
		linear(ls, "Brutus Antony");
		
		System.out.println();


		TermDocumentMatrix tdm = new TermDocumentMatrix(corpus);
		termDocumentMatrix(tdm, "Brutus");
		termDocumentMatrix(tdm, "Brutus Antony");
	
		System.out.println();
		
		// Demo mit Reden von Barack Obama
		String corpusDir = "data/obama";
		corpus = new Corpus(corpusDir);
		System.out.println("Corpus: Barack Obama");
		System.out.println("Corpus work size: " + corpus.getWorks().size());

		// zuerst die naive lineare Suche
		LinearSearch ls2 = new LinearSearch(corpus);
		linear(ls2, "Freedom");
		linear(ls2, "Freedom Patriot");
		
		System.out.println();
		
		// TermDocMatrix
		TermDocumentMatrix tdm2 = new TermDocumentMatrix(corpus);
		termDocumentMatrix(tdm2, "Freedom");
		termDocumentMatrix(tdm2, "Freedom Patriot");
		
		// Matrix ausgeben
		//to do
		//tdm. ...
	}

	public static void linear(InformationRetrieval ls, String query) {
		System.out.println("-------------------");
		System.out.println("Test Linear Search");
		System.out.println("-------------------");

		test(ls, query);
	}

	public static void termDocumentMatrix(InformationRetrieval tdm, String query) {
		System.out.println("------------------------------");
		System.out.println("Test Term-Document-Matrix");
		System.out.println("------------------------------");

		test(tdm, query);
	}

	public static void test(InformationRetrieval ir, String query) {
		long start = System.currentTimeMillis();
		HashSet<Integer> result1 = (HashSet<Integer>) ir.search(query);
		long end = System.currentTimeMillis();
		System.out.println("Search: " + (end - start) + " ms.");
		System.out.println("Result size for query " + query + ": "
				+ result1.size());
	}
}