package de.hsog.ir.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import de.hsog.ir.termdocmatrix.Corpus;

/**
 * Erstellt aus einem Text-Dokumente mehrere
 * einzelne Dokumente
 * @author Trahasch
 *
 */
public class Splitter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int i = 0;
		Corpus corpus = new Corpus("C://irworking1//shaks12.txt", "1[56][0-9][0-9]");

		for (String work : corpus.getWorks()) {
			work = work.trim();
			PrintWriter out = openWriter("C://irworking1//doc" + i + ".txt");
			out.println(work);
			out.close();
			i = i +1;
		}

	}

	private static PrintWriter openWriter(String name) {
		try {
			File file = new File(name);
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(file)), true);
			return out;
		} catch (IOException e) {
			System.out.println("I/O Error");
			System.exit(0);
		}
		return null;
	}
}